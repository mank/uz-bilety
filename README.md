This is a bash script bundled with Jjencode-java solution (http://github.com/amatkivskiy/Jjencode) to check for tickets on UZ site.

Dependencies:
sudo apt-get install jq httpie
You will also need java 1.7 ( for Jjencode to work, it is compiled and inlcuded in repo. )

Usage:

Clone or download repo into separate folder.
Cd to that folder, and run:

./bilety.sh Київ Івано-Франківськ 11.07.2016

Script takes 1st parameter as From, 2nd parameter as To, 3rd parameter - ride date is optional, if not entered - current (system) date is used.

In case script needs to be linked and run from any directory, edit 2 first lines in  "bilety.sh" to contain full path to script:
E.g:
export TMP_FILES_LOC=/home/mank/uz-bilety/log
export JJENCODER_PATH=/home/mank/uz-bilety/Jjencode-master/target

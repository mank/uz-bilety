#!/bin/bash
test -e ~/.bilety_path || echo $PWD > ~/.bilety_path;  
SCRIPT_PATH=`cat ~/.bilety_path`
echo "$SCRIPT_PATH/bilety.sh"
################################## set-script-run-path ##########################
RUN=`date +%s`
mkdir $SCRIPT_PATH/log/$RUN

export TMP_FILES_LOC=$SCRIPT_PATH/log/$RUN
export JJENCODER_PATH=$SCRIPT_PATH/Jjencode-master/target

# TODO: exclude seats
# for i in "$@"
# do
# case $i in
#     -e=*|--exclude=*)
#     IFS=';, ' read -a EXLUDED_SEATS <<< `echo $i | sed 's/[-a-zA-Z0-9]*=//'`
#     shift
#     ;; 
#     *)
#             # unknown option
#     ;;
# esac
# done
# echo ${EXLUDED_SEATS[@]}

#1 and 2nd args are from-to. If empty - defaults are Kyiv-IvFr.
FROM=${1-Київ}
TO=${2-Івано-Франківськ}
#3rd argument - ride date (optional, defaults to sysdate). Format is DD.MM.YYYY
DATE=${3-`date +%d.%m.%Y`}
FROM_TIME=${4-00:00}
TILL_TIME=$5





control_c()
# run if user hits control-c
{
  echo "*** Ouch! Exiting ***"
  exit 1
}
 
# trap keyboard interrupt (control-c)
trap control_c SIGINT

urlencode() { 
    # urlencode <string>
    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%s' "$c" | xxd -p -c1 |
                   while read c; do printf '%%%s' "$c"; done ;;
        esac
    done
#taken from https://gist.github.com/cdown/1163649
}

get_from_to_param() {
FROM_CODE=$(http http://booking.uz.gov.ua/purchase/station/`urlencode $FROM`/ | jq ".value | .[] | select(.title == \"$FROM\") | .station_id" | tr -d '"')
TO_CODE=$(http http://booking.uz.gov.ua/purchase/station/`urlencode $TO`/ | jq ".value | .[] | select(.title == \"$TO\") | .station_id" | tr -d '"')


if [[ -z "$FROM_CODE" || -z "$TO_CODE" ]]; then
  echo "could not fetch station(s) id. Check spelling/language."
  exit 1
fi
}

get_from_to_param;

PARAMS=("station_id_from=$FROM_CODE"
	    "station_id_till=$TO_CODE"
	    "station_from=$FROM"
	    "station_till=$TO"
	    "date_dep=$DATE"
	    "time_dep=$FROM_TIME"
	    "time_dep_till=$TILL_TIME"
	    "another_ec=0"
	    "search="
	   )

echo -e "\nSeaching for a ride from $FROM:$FROM_CODE to $TO:$TO_CODE (departure $DATE from $FROM_TIME to $TILL_TIME)\n"

#Try find tickets, in loop for every 1-2 minutes (random value of seconds between 60-120) and exit if found.
while true; do
#remove previous http session.
rm -f ~/.httpie/sessions/booking.uz.gov.ua/$RUN.json;

echo -e "########## $(date) #########\n$FROM - $TO (departure $DATE from $FROM_TIME to $TILL_TIME)"

HEADERS=("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
	     "Accept-Encoding: gzip, deflate"
	     "Accept-Language: en-US,en;q=0.5"
	     "Connection: keep-alive"
	     "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0"
	     )

http --session=$RUN --print=hbHB -o $TMP_FILES_LOC/get_`date +%s` http://booking.uz.gov.ua/en/ "${HEADERS[@]}"
LATEST_GET=`ls -1t $TMP_FILES_LOC/get_*| head -1`

#Parse JS Encoded token from page
ENC_DATA=$(tail -10 $LATEST_GET|grep -oP "_gaq\.push\(\[\'_trackPageview\'\]\)\;.*\(function \(\) \{var ga"|sed -e 's/(function () {var ga//g'|sed -e "s/_gaq.push(\['_trackPageview'\]);//g");
DEC_DATA=$(cd $JJENCODER_PATH && java -classpath "commons-io-2.4.jar:commons-lang3-3.1.jar:" com.jjencoder.JJencoder $ENC_DATA );
GV_TOKEN=$(echo $DEC_DATA |grep -oP '[\w]{13,50}');

#Add custom headers:
HEADERS=("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
         "Accept-Encoding: gzip, deflate"
         "Accept-Language: en-US,en;q=0.5"
         "Connection: keep-alive"
         "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0"
         "Content-Type: application/x-www-form-urlencoded"
         "DNT: 1"
         "GV-Ajax: 1"
         "GV-Referer: http://booking.uz.gov.ua/"
         "GV-Screen: 1920x1080"
         "GV-Token: $GV_TOKEN"
         "Referer: http://booking.uz.gov.ua/"
        )
#sleep a bit to simulate normal user behaviour.
sleep 2;

#use --print=hbHB for full httpie output
http --session=$RUN --print=b -o $TMP_FILES_LOC/resp_`date +%s` --form http://booking.uz.gov.ua/purchase/search/ "${HEADERS[@]}"  "${PARAMS[@]}"

LATEST_RESP=$(ls -1t $TMP_FILES_LOC/resp_*| head -1)

echo 'Response:'
#cat $LATEST_RESP     #uncomment for debug mode
#use jq to parse json.  https://stedolan.github.io/jq
echo -e $(tail -2 $LATEST_RESP) | jq .

POEZD=$(echo -e `tail -2 $LATEST_RESP` | jq '.value' | jq '.[0].num' 2>/dev/null)

if [ -z $POEZD ]; then
  #do nothing.
  SLEEP=$(shuf -i60-120 -n1)
  echo "sleeping for $SLEEP secs"
  sleep $SLEEP   #random from 60 to 120
else
  #got tickets.
  #notify to tray - linux mint
  echo "Reserve on http://booking.uz.gov.ua !"
  notify-send "Places found. `date`"

  #notify in telegram, requires installed telegram-cli. Instructions available at https://habrahabr.ru/company/centosadmin/blog/303490/
  /usr/src/tg/bin/telegram-cli -k /usr/src/tg/tg-server.pub -U root -W -e "msg Olesya Places found: $(echo `tail -2 $LATEST_RESP | jq . | tr -d '{[]},"' | sed -e '/^ *$/d'`)" > /dev/null && echo 'Sent to Telegram.'
  exit 0
fi

done